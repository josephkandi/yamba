package za.peruzal.yamba;


import android.os.AsyncTask;
import android.util.Log;

import winterwell.jtwitter.Twitter;


/**
 * Created by Joseph on 2013/06/12.
 */
public class PostToTwitter extends AsyncTask<String, Void ,Boolean> {
    private final static String TAG = "PostToTwitter";

    @Override
    protected Boolean doInBackground(String... params) {
        final Twitter twitter = new Twitter("student", "password");
        twitter.setAPIRootUrl("http://yamba.marakana.com/api");
        final String statusText = params[0];
        twitter.setStatus(statusText);
        return true;
    }


    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if(result){
            Log.d(TAG, "Successfully posted to Twitter");
        }else{
            Log.d(TAG, "Failed to update status");
        }
    }
}
