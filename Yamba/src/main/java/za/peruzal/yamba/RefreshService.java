package za.peruzal.yamba;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.util.List;

import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;

/**
 * Created by Joseph on 2013/06/13.
 */
public class RefreshService extends IntentService {
    private static final String SERVICE_NAME = "RefreshService";
    private static final String TAG = "RefreshService";


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    public RefreshService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
       StatusData db = ((YambaApp)getApplication()).statusData;
        try {
            List<Twitter.Status> statusList = ((YambaApp)getApplication()).getTwitter().getPublicTimeline();

            for (Twitter.Status status : statusList){
                db.insert(status);
                Log.d(TAG, String.format("User : %s, Status : %s", status.user.screenName, status.text));
            }
        } catch (TwitterException e) {
            e.printStackTrace();
        }

    }
}
