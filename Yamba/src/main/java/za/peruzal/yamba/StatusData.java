package za.peruzal.yamba;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import winterwell.jtwitter.Twitter;

/**
 * Created by Joseph on 2013/06/15.
 */
public class StatusData {
    private static final String TAG = "StatusData";

    public static final String C_ID = "_id";
    public static final String C_CREATED_AT = "created_at";
    public static final String C_USER = "user_name";
    public  static final String C_TEXT = "status_text";
    public static final String DB_NAME = "timeline_db";
    public  static final String TABLE_NAME = "status";
    public static final int VERSION = 1;

    Context context;
    DBHelper dbHelper;
    SQLiteDatabase db;

    public StatusData(Context context) {
        this.context = context;
        this.dbHelper = new DBHelper();
    }

    private class DBHelper extends SQLiteOpenHelper{
        private DBHelper() {
            super(context, DB_NAME , null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String sql;
            sql = String.format("CREATE TABLE %s" +
                    "(%s int primary key, %s int, %s text, %s text)",
                    TABLE_NAME, C_ID, C_CREATED_AT, C_USER, C_TEXT);
            Log.d(TAG, "onCreate with SQL " + sql);
            db.execSQL(sql);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            String sql = String.format("DROP IF EXISTS %s", TABLE_NAME);
            Log.d(TAG, sql);
            db.execSQL(sql);
            onCreate(db);
        }
    }

    public void insert(Twitter.Status status){
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(C_ID, status.id);
        values.put(C_CREATED_AT, status.createdAt.getTime());
        values.put(C_USER, status.user.name);
        values.put(C_TEXT, status.text);

        Log.d(TAG, "inserting into status");
        //db.insert(TABLE_NAME, null, values);
        db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
    }
    public void read(){

    }
}
