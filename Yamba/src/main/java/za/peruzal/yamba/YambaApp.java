package za.peruzal.yamba;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import winterwell.jtwitter.Twitter;

/**
 * Created by Joseph on 2013/06/13.
 */
public class YambaApp extends Application implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "YambaApp";
    Twitter twitter;
    SharedPreferences prefs;
    StatusData statusData;


    public Twitter getTwitter() {
        if(twitter == null)
        {
            String username = prefs.getString("username", "");
            String password = prefs.getString("password", "");
            String server = prefs.getString("server", "");
            twitter = new Twitter(username, password);
            twitter.setAPIRootUrl(server);
        }

        return twitter;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreated");
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        prefs.registerOnSharedPreferenceChangeListener(this);

        statusData = new StatusData(this);

        Log.d(TAG, prefs.getString("server", "No server defined"));
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "Preferences changed " + key);
        twitter = null;
    }
}
