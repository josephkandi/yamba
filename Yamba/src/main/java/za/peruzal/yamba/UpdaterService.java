package za.peruzal.yamba;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.List;

import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;

/**
 * Created by Joseph on 2013/06/12.
 */
public class UpdaterService extends Service {
    private static final String TAG = "UpdaterService";
    boolean isRunning = false;
    Twitter twitter;
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        twitter = ((YambaApp)getApplication()).getTwitter();
        Log.d(TAG, "onCreate");
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        isRunning = true;
        final int delay = Integer.parseInt(((YambaApp) getApplication()).prefs.getString("delay", "30"));
        Log.d(TAG, "Delay is " + delay);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(isRunning){
                try {
                    List<Twitter.Status> statusList;
                    statusList = twitter.getPublicTimeline();

                    for (Twitter.Status status : statusList){
                        ((YambaApp)getApplication()).statusData.insert(status);
                        Log.d(TAG, String.format("User : %s, Status : %s", status.user.screenName, status.text));
                    }
                    Thread.sleep(delay * 1000);
                } catch (TwitterException e){
                    Log.e(TAG, "Error " + e.getMessage());
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                }
            }
        }).start();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        Log.d(TAG, "onDestroy");
    }
}
