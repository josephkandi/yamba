package za.peruzal.yamba;

import android.os.Bundle;
import android.preference.PreferenceActivity;


/**
 * Created by Joseph on 2013/06/13.
 */
public class PrefsActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);
    }
}
