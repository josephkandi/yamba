package za.peruzal.yamba;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Joseph on 2013/06/15.
 */
public class BootReceiver extends BroadcastReceiver {

    private static final String TAG = "BootReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
       context.startService(new Intent(context, UpdaterService.class));
        Log.d(TAG, "onReceive");
    }
}
