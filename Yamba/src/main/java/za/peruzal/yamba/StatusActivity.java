package za.peruzal.yamba;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class StatusActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "StatusActivity";
    Button buttonUpdate;
    EditText editStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        buttonUpdate = (Button)findViewById(R.id.button_update);
        editStatus = (EditText)findViewById(R.id.edit_status);

       buttonUpdate.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.status, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        PostToTwitter twitter = new PostToTwitter();
        final String statusText = editStatus.getText().toString();

        try {
             twitter.execute(statusText);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Updated status to " + statusText);
        Toast.makeText(getBaseContext(), "Status updated with " + statusText, Toast.LENGTH_LONG).show();
        Log.d(TAG, "onClicked with text " + statusText);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intentUpdater = new Intent(getBaseContext(), UpdaterService.class);
        Intent intentRefresh = new Intent(getBaseContext(), RefreshService.class);
        Intent intentPrefs = new Intent(getBaseContext(), PrefsActivity.class);
        switch(item.getItemId()){
            case R.id.item_start_service:
                startService(intentUpdater);
                return true;
            case R.id.item_stop_service:
                stopService(intentUpdater);
                return true;
            case R.id.item_refresh_service:
                startService(intentRefresh);
                return true;
            case R.id.item_prefs:
                startActivity(intentPrefs);
                return true;
            default:
                return false;
        }
    }
}
